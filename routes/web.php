<?php

use App\Http\Middleware\VerifyBusinessOwner;
use \App\Http\Middleware\VerifyLoggedIn;
use \App\Http\Middleware\VerifyAdmin;

//Page requests

Route::get('/', 'PagesController@index');

Route::get('/map', 'PagesController@map');

Route::get('/map/{address}', 'PagesController@mapAddress');

Route::get('/listing/{slug}', 'ListingsController@listing');

Route::get('/add-business', 'PagesController@addBusiness')->middleware(VerifyLoggedIn::class);

Route::get('/edit-business/{slug}', 'PagesController@editBusiness')->middleware(VerifyBusinessOwner::class);

Route::get('/profile/{id}', 'PagesController@profile');

Route::get('/faq/', 'PagesController@faq');

Route::get('/admin/', 'PagesController@admin')->middleware(VerifyAdmin::class);

//Ajax requests

Route::post('/featuredListings', 'ListingsController@featuredListingAtLocation');

Route::post('/bounds', 'ListingsController@listingsWithinBounds');

Route::post('/add-new-business', 'BusinessController@addBusiness');

Route::post('/update-business', 'BusinessController@updateBusiness');

Route::post('/approve-listing/', 'ListingsController@approveListing');

Route::post('/reject-listing/', 'ListingsController@rejectListing');

Route::post('/claim-listing/', 'ListingsController@claimListing');

Route::post('/favorite-listing/', 'ListingsController@setFavorite');

//Authentication requests (Ajax)

Auth::routes();

//Helpers (do not uncomment unless necessary)
//Route::get('/undo', 'HelperController@undo');
//Route::get('getImages', 'HelperController@getImages');
//Route::get('insertEmails', 'HelperController@insertEmails');
//Route::get('exportEmails', 'HelperController@exportEmailNames');
//Route::get('addresses', 'HelperController@getAddresses');
//Route::get('body', 'HelperController@getMenuBodyName');