@extends ("layouts.layout")

@section('title', 'Leaftopia')
@section('javascript') <script src="/assets/js/listings.js"></script> @stop

@section('content')
    <?php
    /**
     * Counts the passed menu items and returns the amount of menu
     * items that have title text.
     * @param $menuItems Menu items to count. Must be an associative array from menu_items table
     * @return amount of menu items that were valid
     */
    function getValidMenuItemsAmount($menuItems) {
        $count = 0;

        foreach ($menuItems as $item) {
            if ($item->name !== "") $count++;
        }

        return $count;
    }

    /**
     * Formats day of the week text from passed date array.
     * Eg: mon -> Monday
     * @param $date Associative date array from the database
     * @return formatted day of the week in string form
     */
    function getDayOfWeekFromDate($date) {
        switch ($date["day_of_the_week"]) {
            case "sun": return "Sunday";
            case "mon": return "Monday";
            case "tue": return "Tuesday";
            case "wed": return "Wednesday";
            case "thu": return "Thursday";
            case "fri": return "Friday";
            case "sat": return "Saturday";
            default: return ""; //Should never reach this
        }
    }

    /**
     * Checks passed associative social array for social
     * networks. Returns associative array of social networks
     * with their respective names and links. Formatted as follows:
     * [0 => ["Facebook" => "URL"]]
     */
    function getSocialInformation($social) {
        if (empty($social) || count($social) === 0) return [];

        $newArr = [];
        if ($social[0]->facebook !== "") $newArr["fa-facebook"] = $social[0]->facebook;
        if ($social[0]->twitter !== "") $newArr["fa-twitter"] = $social[0]->twitter;
        if ($social[0]->instagram !== "") $newArr["fa-instagram"] = $social[0]->instagram;
        if ($social[0]->website !== "") $newArr["fa-link"] = $social[0]->website;

        return $newArr;
    }

    ?>

    <div class="header-dark">
        <div class="container">
            @include('layouts.header')
        </div>
    </div>
    <div class="dark-transp">
        <div class="listing-header">
            <div class="container">
                <img src="/avatars/{{ $listing[0]->id }}.jpg" />
                <h4>{{ $listing[0]->name }}</h4>
                <p>{{ $listing[0]->address }}, {{ $listing[0]->city }} {{ $listing[0]->state }}</p>
            </div>
        </div>
        <div id="live-map"></div>
    </div>
    @if($listing[0]->body != "")
        <section class="info-about">
            <div class="container">
                <h3>About</h3>
                <div class="twelve columns">
                    <p> {{ $listing[0]->body }} </p>
                </div>
            </div>
        </section>
    @endif

    {{-- Determine which options to display --}}
    <?php
    $showMenu = getValidMenuItemsAmount($menu) > 0;
    $showDeals = false; //TODO: Implement showing deals once it's in the database
    $showReviews = false; //TODO: Implement showing reviews once it's in the database
    $showPhotos = false; //TODO: Show photos once in database
    $showDetails = true;
    ?>

    <section class="light no-padding" style="padding-bottom: 100px;">
        <div class="listing-options clearfix">
            <div class="container">
                @if ($showDetails)<h4 class="selected" associated="listing-option-details">Details</h4> @endif
                @if ($showMenu) <h4 associated="listing-option-menu">Menu</h4> @endif
                @if ($showDeals)<h4 associated="listing-option-deals">Deals</h4> @endif
                @if ($showReviews)<h4 associated="listing-option-reviews">Reviews</h4> @endif
                @if ($showPhotos)<h4 associated="listing-option-photos">Photos</h4> @endif

                @if(Auth::check())
                    <?php $className = $favorite ? "selected" : "" ?>
                    <a id="favorite-listing" class="{{ $className }}" slug="{{ $listing[0]->slug }}">
                        <span class="fa fa-heart fa-2x"></span>
                    </a>
                @endif
            </div>
        </div>
        <div class="listing-option listing-option-menu">
            <div class="container">
                @if ($showMenu) {{-- Display menu items --}}
                <?php
                $count = 0;
                $max = 4;
                ?>

                @for ($i = 0; $i < count($menu); $i++)
                    @if ($menu[$i]->name === "")
                        @continue
                    @endif

                    <?php $insertRow = $count === 0; ?>
                    @if ($insertRow)
                        <div class="row">
                            @endif
                            <div class="three columns menu-item">
                                <div class="menu-item-img" style="background-image: url('/images/weed.jpg');"></div>
                                <a href="#">
                                    <div class="menu-item-info">
                                        <h6>{{ isset($menu[$i]->name) ? $menu[$i]->name : "" }}</h6>
                                        <p>{{ isset($menu[$i]->body) ? $menu[$i]->body : "" }}</p>
                                    </div>
                                </a>
                            </div>

                            <?php $count ++; ?>
                            @if ($count >= $max || $i + 1 == count($menu)) {{-- Close row div --}}
                        </div>
                        <?php $count = 0; ?>
                    @endif
                @endfor
                @endif
            </div>
        </div>
        <div class="listing-option listing-option-deals">
            <div class="container">
                <div class="row">
                    <div class="twelve columns deal-box">
                        <h5>Here is an example deal</h5>
                        <h6>$20</h6>
                        <p>This is a longer description of what this deal could be. Is is better prices on something for first time patients? Who knows.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="listing-option listing-option-reviews"></div>
        <div class="listing-option listing-option-photos"></div>
        <div class="listing-option listing-option-details">
            <div class="container">
                <div class="row">
                    <div class="five columns listing-details-box">
                        <div class="deal-box text-center">
                            <!-- Hours -->
                            <div class="row">
                                <div class="six columns u-pull-left">
                                    @foreach (json_decode($listing[0]->hours, true) as $day)
                                        @if ($day["opening_time"] != null)
                                            <h6>{{ getDayOfWeekFromDate($day) }}</h6>
                                        @endif
                                    @endforeach
                                </div>
                                <div class="six columns u-pull-right">
                                    @foreach (json_decode($listing[0]->hours, true) as $day)
                                        @if ($day["opening_time"] != null)
                                            <p>{{ $day["opening_time"] }} to {{ $day["closing_time"] }}</p>
                                        @endif
                                    @endforeach
                                </div>
                            </div>

                            <!-- Contact section -->
                            <hr />
                            <div class="row">
                                <?php $contacts = [
                                        "Address" =>  $listing[0]->address . ", " . $listing[0]->city . " " . $listing[0]->state,
                                        "License" => $listing[0]->license_type,
                                        "Email" => $listing[0]->email_address,
                                        "Phone" => $listing[0]->phone_number
                                ]; ?>

                                @foreach ($contacts as $name => $value)
                                    @if ($value !== "")
                                        <div>
                                            <div class="six columns u-pull-left">
                                                <h6>{{ $name }}</h6>
                                            </div>
                                            <div class="six columns u-pull-left">
                                                <p>{{ $value }}</p>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>

                        <!-- Social section -->
                        <?php $socialInfo = getSocialInformation($social); ?>
                        @if (count($socialInfo) > 0)
                            <hr />
                            <div class="social-icons row">
                                @foreach($socialInfo as $service => $value)
                                    <a class="social-icon" href="{{ $value }}">
                                        <span class="fa fa-2x {{ $service }}"></span>
                                    </a>
                                @endforeach
                            </div>
                        @endif
                    </div>


                    <!-- Map -->
                    <div class="seven columns">
                        <div id="live-map-interactive"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- JS -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCCV0UScujyBhP1js2g5oLjV_ss9mOJiRc&callback=initMap" async defer></script>
    <script>
        var map;
        function initMap() {
            var lat = "{{ $listing[0]->latitude }}";
            var long = "{{ $listing[0]->longitude }}";

            //noinspection JSUnresolvedVariable,JSUnresolvedFunction
            map = new google.maps.Map(document.getElementById('live-map'), {
                center: {lat: parseFloat(lat) , lng: parseFloat(long)},
                zoom: 8
            });

            //noinspection JSUnresolvedVariable,JSUnresolvedFunction
            map = new google.maps.Map(document.getElementById('live-map-interactive'), {
                center: {lat: parseFloat(lat) , lng: parseFloat(long)},
                zoom: 14
            });
        }

        //Prepare menu items
        $(document).ready(function() {
            //Show selected
            var associatedName = $(".listing-options .selected").attr("associated");
            $("." + associatedName).css("display", "block");

            $(".listing-options h4").click(function() {
                var $header = $(this);
                var associated = $header.attr("associated");

                //Hide all first
                $(".listing-option").each(function() {
                    $(this).css("display", "none");
                });

                //Deslect all menu items
                $(".listing-options h4").each(function() {
                    $(this).removeClass("selected");
                });

                //Display clicked
                $header.addClass("selected");
                $("." + associated).css("display", "block");
            });

            //Toggle favorite icon
            $("#favorite-listing").click(function() {
                var slug = $(this).attr("slug");

                if ($(this).hasClass("selected")) { //Deselect
                    $(this).removeClass("selected");
                    setListingFavorited(slug, false);
                } else { //Select
                    $(this).addClass("selected");
                    setListingFavorited(slug, true);
                }
            });
        });
    </script>
@stop