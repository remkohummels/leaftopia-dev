@extends('layouts.layout', array("showFooter" => true))

@section('title', 'Leaftopia')
@section('javascript') <script src="assets/js/listings.js"></script> @stop

@section('content')
    <section class="header no-padding">
        <div class="container">
            @include('layouts.header', array("hideSearch" => true))
            <div class="row">
                <div class="twelve columns">
                    <h2>Find a Cannabis Dispensary or Doctor near you.</h2>
                    <div class="search-container">
                        <div class="row">
                            <div class="two-thirds column">
                                <input id="map-search" type="text" placeholder="" />
                            </div>
                            <div class="one-third column">
                                <button onclick="search()" class="button full">Search</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="light"> <!-- Dispensary Listings -->
        <div class="container dispensary">
            <h3>
                <span><img src="images/i-medical-black.png"></span>
                <strong>Featured Dispensaries</strong>
            </h3>
            <div class="row listings">
                <div class="three columns">
                    <div class="filler-listing"></div>
                </div>
                <div class="three columns">
                    <div class="filler-listing"></div>
                </div>
                <div class="three columns">
                    <div class="filler-listing"></div>
                </div>
                <div class="three columns">
                    <div class="filler-listing"></div>
                </div>
            </div>
        </div>
    </section>
    <section class="light"> <!-- Doctor Listings -->
        <div class="container doctor">
            <h3>
                <span><img src="images/i-doctor-black.png"></span>
                <strong>Featured Doctors</strong>
            </h3>
            <div class="row listings">
                <div class="three columns">
                    <div class="filler-listing"></div>
                </div>
                <div class="three columns">
                    <div class="filler-listing"></div>
                </div>
                <div class="three columns">
                    <div class="filler-listing"></div>
                </div>
                <div class="three columns">
                    <div class="filler-listing"></div>
                </div>
            </div>
        </div>
    </section>
    <section class="light listing-bottom"> <!-- Product Listings -->
        <div class="container product">
            <h3>
                <span><img src="images/i-delivery-black.png"></span>
                <strong>Featured Products</strong>
            </h3>
            <div class="row listings">
                <div class="three columns">
                    <div class="filler-listing"></div>
                </div>
                <div class="three columns">
                    <div class="filler-listing"></div>
                </div>
                <div class="three columns">
                    <div class="filler-listing"></div>
                </div>
                <div class="three columns">
                    <div class="filler-listing"></div>
                </div>
            </div>
        </div>
    </section>

    <!-- JS -->
    <script>
        $(document).ready(function() {
            var listingAmt = 4;

            getCurrentLocation(function(location) {
                getMapImageAtLocation(location, '#map'); //Create map at top
                getNameAtLocation(location, function(name) { //Get name of location
                    $("#map-search").attr("placeholder", name);
                });
                getFeaturedListingForLocation(location, listingAmt, function(data){
                    $(".listings").empty();

                    for (var i = 0; i < data.length; i++) {
                        var $template = $("<div class='three columns'>" +
                                "<div class='listing'>" +
                                "<a href='#' class='listing-slug'>" +
                                "<div class='listing-image'></div>" +
                                "<div class='listing-details'>" +
                                "<div>Some Company Name</div>" +
                                "<p><span class='fa fa-globe'></span>Boston, MA</p>" +
                                "<p><span class='fa fa-location-arrow'></span>1.3 Miles</p>" +
                                "</div>" +
                                "</a>" +
                                "</div>" +
                                "</div>");

                        var rounded = (Math.round(data[i].distance * 10) / 10) + " Miles";
                        var milesCache = $template.find(".listing-details p:last").children();
                        $template.find(".listing-details p:last").text(rounded).prepend(milesCache);

                        //Name
                        $template.find(".listing-details div").text(data[i].name);

                        //noinspection JSUnresolvedVariable
                        getMapImageAtLocation(data[i].lat_lon, $template.find(".listing-image")); //Apply map image of location

                        //noinspection JSUnresolvedVariable
                        var cityState = data[i].city + ", " + data[i].state; //City & State
                        var cityCache = $template.find(".listing-details p:first").children();
                        $template.find(".listing-details p:first").text(cityState).prepend(cityCache);

                        //Set listing link
                        //noinspection JSUnresolvedVariable
                        var link = "/listing/" + data[i].slug;
                        $template.find(".listing-slug").attr("href", link);

                        $(".listings").append($template);
                    }

                    // Dispensary
                    $(".dispensary").find(".columns:first").find(".listing-image").append(
                        "<div class='dispensary-gold' style='width:54px;height:60px;margin:40px auto;'></div>"
                    );
                    $(".dispensary").find(".columns:nth-child(2)").find(".listing-image").append(
                        "<div class='dispensary-silver' style='width:54px;height:60px;margin:40px auto;'></div>"
                    );
                    $(".dispensary").find(".columns:nth-child(3)").find(".listing-image").append(
                        "<div class='dispensary-bronze' style='width:54px;height:60px;margin:40px auto;'></div>"
                    );
                    $(".dispensary").find(".columns:last").find(".listing-image").append(
                        "<div class='dispensary-plus' style='width:54px;height:60px;margin:40px auto;'></div>"
                    );

                    // Doctor
                    $(".doctor").find(".columns:first").find(".listing-image").append(
                        "<div class='doctor-gold' style='width:54px;height:60px;margin:40px auto;'></div>"
                    );
                    $(".doctor").find(".columns:nth-child(2)").find(".listing-image").append(
                        "<div class='doctor-silver' style='width:54px;height:60px;margin:40px auto;'></div>"
                    );
                    $(".doctor").find(".columns:nth-child(3)").find(".listing-image").append(
                        "<div class='doctor-bronze' style='width:54px;height:60px;margin:40px auto;'></div>"
                    );
                    $(".doctor").find(".columns:last").find(".listing-image").append(
                        "<div class='doctor-plus' style='width:54px;height:60px;margin:40px auto;'></div>"
                    );

                    // Product
                    $(".product").find(".columns:first").find(".listing-image").append(
                        "<div class='product-gold' style='width:54px;height:60px;margin:40px auto;'></div>"
                    );
                    $(".product").find(".columns:nth-child(2)").find(".listing-image").append(
                        "<div class='product-silver' style='width:54px;height:60px;margin:40px auto;'></div>"
                    );
                    $(".product").find(".columns:nth-child(3)").find(".listing-image").append(
                        "<div class='product-bronze' style='width:54px;height:60px;margin:40px auto;'></div>"
                    );
                    $(".product").find(".columns:last").find(".listing-image").append(
                        "<div class='product-plus' style='width:54px;height:60px;margin:40px auto;'></div>"
                    );
                });
            });
        });
    </script>
@stop