<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Auth;
use App\Custom\LatLonDistance;
use Illuminate\Http\Request;
use DB;

class ListingsController extends Controller
{
    protected $request;

    public function __construct(Request $request) {
        $this->request = $request;
    }

    public function featuredListingAtLocation() {
        if ($this->request->ajax()) {
            $data = $this->request->all();

            $latitude = $data["lat"];
            $longitude = $data["lon"];
            $amount = $data["amount"];

            $sqlStatement = "SELECT *, ( 3959 * acos( cos( radians($latitude) ) * cos( radians( latitude ) ) * " .
                "cos( radians( longitude ) - radians($longitude) ) + sin( radians($latitude) ) * " .
                "sin( radians( latitude ) ) ) ) AS distance FROM crawled_data ORDER BY distance LIMIT $amount;";

            $results = DB::select($sqlStatement);
            $finalArray = array();

            foreach($results as $result) {
                $latLon = $result->lat_lon;
                $name = $result->name;
                $city = $result->city;
                $state = $result->state;
                $slug = $result->slug;
                $distance = LatLonDistance::distance(floatval($latitude), floatval($longitude), floatval($result->latitude), floatval($result->longitude), "M");

                $jsonArr = [
                    "lat_lon" => $latLon,
                    "name" => $name,
                    "distance" => $distance,
                    "city" => $city,
                    "state" => $state,
                    "slug" => $slug
                ];
                array_push($finalArray, $jsonArr);
            }

            $encodedJson = json_encode($finalArray);
            echo($encodedJson);
        } else {
            echo("Request should only be made with ajax");
        }
    }

    public function listing($slug) {
        $isFaved = false;
        if (Auth::check()) {
            $favedListings = DB::table('favorites')->where('user_id', Auth::user()->id)->get();

            foreach($favedListings as $listing) {
                if ($listing->listing_slug == $slug) {
                    $isFaved = true;
                }
            }
        }

        return view('listing',
            ['listing' => DB::table('crawled_data')->where('slug', $slug)->get(),
                'menu' => DB::table('menu_items')->where('listing_slug', $slug)->get(),
                'social' => DB::table('social')->where('slug', $slug)->get(),
                'favorite' => $isFaved]);
    }

    public function listingsWithinBounds() {
        if ($this->request->ajax()) {
            $bounds = $this->request->all()["bounds"];

            $upperLat = $bounds["north"];
            $lowerLat = $bounds["south"];
            $upperLon = $bounds["east"];
            $lowerLon = $bounds["west"];

            $query = "SELECT * FROM crawled_data WHERE (latitude BETWEEN $lowerLat AND $upperLat) AND (longitude BETWEEN $lowerLon AND $upperLon) LIMIT 300";
            $results = DB::select($query);
            if (count($results) > 0) {
                //Returns: title, slug, city, state, latitude, longitude, avatar
                $finalData = [];

                foreach($results as $listing) {
                    $title = $listing->name;
                    $slug = $listing->slug;
                    $city = $listing->city;
                    $state = $listing->state;
                    $latitude = $listing->latitude;
                    $longitude = $listing->longitude;
                    $avatar = "/avatars/" . $listing->id . ".jpg";

                    array_push($finalData, ["title" => $title,
                        "slug" => $slug, "city" => $city, "state" => $state,
                        "latitude" => $latitude, "longitude" => $longitude,
                        "avatar" => $avatar]);
                }

                echo (json_encode($finalData));
            }
        }
    }

    public function approveListing() {
        if ($this->isAuthenticatedAdmin()) {
            $slug = $this->request->all()["slug"];
            DB::table('crawled_data')->where('slug', $slug)->update(['approved' => '1']);

            echo ("1");
        } else {
            die("You must be logged in to perform this operation");
        }
    }

    public function rejectListing() {
        if ($this->isAuthenticatedAdmin()) {
            $slug = $this->request->all()["slug"];
            DB::table('crawled_data')->where('slug', $slug)->delete();

            echo ("1");
        } else {
            die("You must be logged in to perform this operation");
        }
    }

    public function claimListing() {
        if ($this->isAuthenticatedAdmin()) {
            $slug = $this->request->all()["slug"];
            $email = $this->request->all()["email"];

            $userResult = DB::table('users')->where('email', $email)->get();
            $listingResult = DB::table('crawled_data')->where('slug', $slug)->get();

            if (count($listingResult) == 0) {
                die("The listing requested does not exist");
            } else if (count($userResult) == 0) {
                die("No user with the email: " . $email . " was found in the database.");
            } else if ($userResult[0]->id == $listingResult[0]->owner) { //Listing owner and user owner match, done
                die("1"); //Success
            } else if ($listingResult[0]->owner != '0') { //Some other owner set, change
                $currentOwner = DB::table('users')->where('id', $listingResult[0]->owner)->get();

                if (count($currentOwner) != 0) {
                    $currentOwnerListings = json_decode($currentOwner[0]->owned_listings, true);

                    if(($key = array_search($slug, $currentOwnerListings)) !== false) { //Search for slug and unset
                        unset($currentOwnerListings[$key]);
                    }

                    DB::table('users')->where('id', $listingResult[0]->owner)->update(['owned_listings' => json_encode($currentOwnerListings)]);
                }
            }

            //Set new owner
            $owned = json_decode($userResult[0]->owned_listings, true);
            array_push($owned, ["slug" => $slug]);

            DB::table('users')->where('id', $userResult[0]->id)->update(['owned_listings' => json_encode($owned)]); //Set in DB
            DB::table('crawled_data')->where('slug', $slug)->update(['owner' => $userResult[0]->id]); //Update owner on listing

            echo("1");
        } else {
            die("You must be logged in to perform this operation");
        }
    }

    public function setFavorite() {
        $slug = $this->request->all()["slug"];
        $setFaved = $this->request->all()["favorite"];

        if (Auth::check()) {
            if ($setFaved == "true") {
                if (count(DB::table('favorites')
                        ->where(['user_id' => Auth::user()->id])
                        ->where(['listing_slug' => $slug])->get()) == 0) {
                    DB::table('favorites')->insert([
                        'user_id' => Auth::user()->id,
                        'listing_slug' => $slug
                    ]);
                }
            } else {
                DB::table('favorites')
                    ->where(['user_id' => Auth::user()->id])
                    ->where(['listing_slug' => $slug])
                    ->delete();
            }
        } else {
            die("User must be logged in");
        }
    }

    private function isAuthenticatedAdmin() {
        return (Auth::check() && Auth::user()->level == 1);
    }
}