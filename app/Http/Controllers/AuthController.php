<?php
/**
 * Created by PhpStorm.
 * User: CChristie
 * Date: 11/9/2016
 * Time: 6:52 PM
 */

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use DB;

class AuthController extends Controller {
    public function loginUser() {
        $email = Input::get("email");
        $password = Input::get("password");

        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            return redirect()->intended('home');
        } else {
            echo ("The email address or password that you entered is incorrect.");
        }
    }

    public function registerUser() {
        $email = Input::get("email");
        $password = Input::get("password");

        
    }
}