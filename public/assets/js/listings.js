/**
 * Created by CChristie on 8/3/2016.
 */

var key = "AIzaSyCCV0UScujyBhP1js2g5oLjV_ss9mOJiRc";

function getMapImageAtLocation(latLon, selector) {
    getMapImageAtLocationCallback(latLon, null, function(url, ele) {
        $(selector).css("background-image", "url('" + url + "')");
    });
}

function getMapImageAtLocationCallback(latLon, element, callback) {
    var size = [422, 150]; //Width / Height
    var dim = Math.round(size[0]) + "x" + Math.round(size[1]);
    var scale = "2";
    var url = "https://maps.googleapis.com/maps/api/staticmap?center=" + latLon + "&zoom=12"
        + "&size=" + dim + "&scale=" + scale + "&key=" + key;

    callback(url, element);
}

function getNameAtLocation(latLon, callback) {
    var url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + latLon + "&key=" + key;

    $.getJSON(url, function(json) {
        //noinspection JSUnresolvedVariable
        var addresses = json.results[0].address_components;
        var city = "";
        var state = "";

        $.each(addresses, function(i, addressComponent) {
            if (addressComponent.types[0] == "locality") { //City
                //noinspection JSUnresolvedVariable
                city = addressComponent.long_name;
            } if (addressComponent.types[0] == "administrative_area_level_1") { //State
                //noinspection JSUnresolvedVariable
                state = addressComponent.short_name;
            }
        });

        var formatted = city == "" ? state : city + ", " + state;
        callback(formatted);
    });
}

/**
 * Geocodes the passed address and returns the latitude and longitude
 * @param addr address to geocode
 * @param callback(Array) lat / lng array [lat, lng]. Undefined if failed to geocode
 */
function geocodeAddress(addr, callback) {
    var addrURL = encodeURI(addr);
    var fullURL = "https://maps.googleapis.com/maps/api/geocode/json?address=" + addrURL + "&key=AIzaSyCCV0UScujyBhP1js2g5oLjV_ss9mOJiRc"

    $.getJSON(fullURL, function(json) { //Retrieve json response
        if (json.results != undefined) {
            callback(json.results[0].geometry.location)
        } else {
            callback(undefined);
        }
    });
}

/**
 * Keeps track of the last lookup location
 */
var currentLocation;

/**
 * @type {boolean} If last location lookup failed, this is true.
 */
var failedToLookupLocation = false;

function getCurrentLocation(callback) {
    var defaultLocation = "40.714224,-73.961452"; //Return NYC if location lookup fails

    if (currentLocation == undefined) {
        if (typeof(navigator.geolocation) != 'undefined') {
            navigator.geolocation.getCurrentPosition(function (location) {
                currentLocation = [location.coords.latitude, location.coords.longitude].join(',');
                failedToLookupLocation = false;
                callback(currentLocation);
            });
        } else {
            currentLocation = defaultLocation;
            failedToLookupLocation = true;
            callback(currentLocation);
        }
    } else {
        callback(currentLocation);
    }
}

var lastCheck = new Date();
/**
 * Watches passed map object and waits for it to change, returning
 * listings pulled from database. Optional delay ensures that the
 * specified amount of seconds has passed before checking the database.
 * @param map Map object to check
 * @param delay seconds to wait before checking db again
 * @param callback(json-response) called when the database query has finished.
 */
function getListingsOnMapChange(map, delay, callback) {
    map.addListener('bounds_changed', function() {
        var timeDifference = (new Date().getTime() - lastCheck.getTime()) / 1000;
        if (timeDifference > delay) { //if x amount of seconds has passed
            lastCheck = new Date();
            getListings(map, callback);
        } else {
            return [];
        }
    });
}

/**
 * Gets listings for current location and updates map
 * This is good for loading the map for the first time
 * @param map Map object to check
 * @param callback Called when the data has loaded
 */
function getListings(map, callback) {
    $.ajax({
        url: '/bounds',
        type: 'POST',
        data: { 'bounds' : map.getBounds().toJSON() },
        dataType: 'json',
        headers: { 'X-CSRF-Token' : $('meta[name="_token"]').attr('content') },
        success: function(data) {
            displayListingData(data, $(".map-listings"));
            callback(data);
        }, error: function(data) {
            console.log("Failed to load map listings");
            callback(data);
        }
    });
}

/**
 * Displays retrieved listing data on map page.
 * Works by appending data to passed element
 * @param data Json data
 * @param $element Element as jQuery object
 */
function displayListingData(data, $element) {
    $element.html(""); //Reset HTML in element

    var $baseElement = $($.parseHTML('' +
        '<div class="map-listing">' +
        '<a href="#" target="_blank">' +
        '<div class="row listing-container">' +
        '<div class="nine columns">' +
        '<h5></h5>' +
        '<p><span class="fa fa-globe"></span></p>' + //City
        '<p><span class="fa fa-location-arrow"></span></p>' + //Distance
        '</div>' +
        '<div class="three columns">' +
        '<img class="map-listing-image" src="/images/logo.jpg" />' +
        '</div>' +
        '</div><hr style="margin: 5px 0" />' +
        '</a>' +
        '</div>'));

    getCurrentLocation(function(location) {
        for (var i in data) {
            var listing = data[i];
            var $listingElement = $baseElement.clone();

            $listingElement.find(".listing-container h5").text(listing.title); //Set title
            $listingElement.find(".map-listing-image").attr("src", listing.avatar); //Set avatar
            $listingElement.find(".listing-container p").eq(0).append(" " + listing.city + ", " + listing.state); //Set city
            $listingElement.find("a").attr("href", "/listing/" + listing.slug); //Set link

            //Find current location and set distance
            var lat = location.split(",")[0];
            var lon = location.split(",")[1];

            if (lat === "40.714224" && lon === "-73.961452") { //Failed lookup
                $listingElement.find(".listing-container p").eq(1).remove();
            } else {
                var distance = Math.round(getDistanceFromLatLonInMiles(lat, lon, listing.latitude, listing.longitude));
                $listingElement.find(".listing-container p").eq(1).append(" " + distance + " Miles");
            }

            $element.append($listingElement);
        }
    });
}

/**
 * Finds the distance in miles between pairs of latitude / longitude points
 * @param lat1 Latitude 1
 * @param lon1 Longitude 1
 * @param lat2 Latitude 2
 * @param lon2 Longitude 2
 * @returns {Number} Distance in miles
 */
function getDistanceFromLatLonInMiles(lat1,lon1,lat2,lon2) {
    function deg2rad(deg) {
        return deg * (Math.PI/180)
    }

    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2-lat1);  // deg2rad below
    var dLon = deg2rad(lon2-lon1);
    var a =
        Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
        Math.sin(dLon/2) * Math.sin(dLon/2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = R * c; // Distance in km
    return d * 0.62137; //Distance in mi
}

/**
 * Sets the listing at the passed slug to favorited
 * in the database based on isFaved
 * @param slug slug of listing to fav or not
 * @param isFaved whether to favorite the listing or not
 */
function setListingFavorited(slug, isFaved) {
    $.ajax({
        url: '/favorite-listing',
        type: 'POST',
        data: { 'slug' : slug, 'favorite' : isFaved },
        headers: { 'X-CSRF-Token' : $('meta[name="_token"]').attr('content') },
        success: function(data) {
            console.log(data);
        }, error: function(data) {
            console.log(data);
        }
    });
}